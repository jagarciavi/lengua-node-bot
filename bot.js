var TelegramBot = require('node-telegram-bot-api');
var request = require('request');

var chatId;

/* 
Google Calendar API things
*/
var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

var SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'calendar-nodejs-quickstart.json';


var options = {
  polling: true
};

var token = process.env.TELEGRAM_BOT_TOKEN || '90407023:AAFvIBoD7jH4caZ4TMVg-csds2j8dD1QZjE';

//var bot = new TelegramBot(token, options);

var port = process.env.PORT;
var host = process.env.IP;
var domain = "https://primero-lengua-jagarciavi.c9.io";

var bot = new TelegramBot(token, {webHook: {port: port, host: host}});
// c9.io webhook
bot.setWebHook(domain+':443/bot'+token);

bot.getMe().then(function (me) {
  console.log('Hi my name is %s!', me.username);
});
bot.on('text', function (msg) {
  chatId = msg.chat.id;
  if (msg.text == '/lista') {
    // Get all events of the calendar
    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
      if (err) {
        console.log('Error loading client secret file: ' + err);
        return;
      }
      // Authorize a client with the loaded credentials, then call the
      // Google Calendar API.
      authorize(JSON.parse(content), listEvents);});
  }
  if (msg.text == '/start') {
    bot.sendMessage(chatId, 'Este bot devuelve todas las tareas con fecha de la asignatura de Lengua de 1º de Bachillerato del IES Leonardo Da Vinci');
  }
});


/* Google Calendar functions 
*/

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Lists the next 10 events on the user's primary calendar.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listEvents(auth) {
  var calendar = google.calendar('v3');
  calendar.events.list({
    auth: auth,
    calendarId: 'm2uj30h9b9evadlnmevlm8h7co@group.calendar.google.com',
    timeMin: (new Date()).toISOString(),
    maxResults: 10,
    singleEvents: true,
    orderBy: 'startTime'
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var events = response.items;
    var salida = "";
    if (events.length === 0) {
      console.log('No upcoming events found.');
    } else {
      console.log('Upcoming 10 events:');
      salida = salida.concat("TAREAS PENDIENTES DE ENTREGA:\n\n");
      for (var i = 0; i < events.length; i++) {
        var event = events[i];
        var start = event.start.dateTime || event.start.date;
        console.log('%s - %s', start, event.summary);
        var res = start.split("-");
        console.log('Fecha: %s', res);
        var fecha = res[2].concat("/", res[1], "/", res[0]);
        console.log('Fecha correcta: %s\n', fecha);
        salida = salida.concat(fecha, ": ", event.summary, "\n");
      }
      /*
      var opts = {
        parse_mode:Markdown
      };
      */
      bot.sendMessage(chatId, salida);
    }
  });
}